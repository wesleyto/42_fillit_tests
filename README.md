### What is this repository for? ###

* Testing 42's Fillit project

### How do I get set up? ###

* Move files into your project root directory
* Run the script

### NOTES ###
* The VBAR and HBAR tests may take a while (1-2mins), but they should be doable with backtracking.