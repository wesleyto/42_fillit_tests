# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test_script.sh                                     :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/02/28 16:23:59 by wto               #+#    #+#              #
#    Updated: 2018/02/28 16:24:00 by wto              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NUM_TESTS=23
NUM_ERRORS=21
NUM_RECOG=18

make re

echo "============================================================"
echo
echo "\tTesting Invalid Files"
echo
echo "============================================================"

for num in $(seq -f "%02g" 0 $NUM_ERRORS)
do
	echo =============================================
	echo tests_invalid/test_$num*
	echo =============================================
	./fillit tests_invalid/test_$num*
	echo
done

echo "============================================================"
echo
echo "\tTesting All Possible Block Patterns"
echo
echo "============================================================"

for num in $(seq -f "%02g" 0 $NUM_RECOG)
do
	echo ==================================================
	echo tests_block_recognition/test_$num*
	echo ==================================================
	./fillit tests_block_recognition/test_$num*
	echo
done

echo "============================================================"
echo
echo "\tTesting Valid Test Cases"
echo
echo "============================================================"

for num in $(seq -f "%02g" 0 $NUM_TESTS)
do
	echo =============================================
	echo tests/test_$num*
	echo =============================================
	./fillit tests/test_$num*
	echo
done

make fclean